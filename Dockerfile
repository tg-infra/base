FROM docker:stable
MAINTAINER Ivan Shapovalov <shapovalov.is@aurrency.com>

RUN : \
	&& apk add --no-cache \
		python3 \
		git \
		jq \
		curl \
		bash \
	&& :

RUN : \
	&& pip3 install --upgrade \
		pip \
	&& pip3 install \
		pyyaml \
		yq \
	&& :
